import logging

from django.utils import timezone

from django.db.models import F, Q
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, generics
from survey.models import Survey, Question, Choice, Responder
from survey.serializers import SurveySerializer, QuestionSerializer, ChoiceSerializer, ResponderSerializer, RespondSerializer, FullSurveySerializer, FullQuestionSerializer, SurveyDetailSerializer
from django.db.models import Count
from django.db import connection

class SurveyList(APIView):

    def get(self, request):
        surveys_detail = Survey.objects.annotate(views=Count('responder'), responses=Count('responder', filter=Q(responder__stage=2)))
        serializer = SurveyDetailSerializer(surveys_detail, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = SurveySerializer(data=request.data)
        print(request.data)
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SurveyDetail(APIView):

    def get_object(self, pk):
        try:
            return Survey.objects.get(pk=pk)
        except Survey.DoesNotExist:
            raise Http404

    def get(self, request, pk):

        survey = Survey.objects.prefetch_related("questions").prefetch_related("questions__choices").get(pk=pk)
        serializer = FullSurveySerializer(survey)
        return Response(serializer.data)


    def put(self, request, pk):
        survey = self.get_object(pk)
        serializer = SurveySerializer(survey, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        survey = self.get_object(pk)
        survey.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class QuestionList(APIView):

    def get(self, request):
        questions = Question.objects.all()
        serializer = QuestionSerializer(questions, many=True)
        return Response(serializer.data)

    def post(self, request, survey_id):
        data = request.data
        data["survey"] = survey_id
        serializer = QuestionSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class QuestionDetail(APIView):

    def get_object(self, pk):
        try:
            return Question.objects.get(pk=pk)
        except Question.DoesNotExist:
            raise Http404

    def get(self, request, survey_id, pk):
        question = self.get_object(pk)
        serializer = FullQuestionSerializer(question[0])
        data = serializer.data
        data["survey"] = survey_id
        return Response(data)

    def put(self, request, survey_id,  pk):
        question = self.get_object(pk)
        data = request.data
        data["survey"] = survey_id
        serializer = QuestionSerializer(question, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, survey_id, pk):
        question = self.get_object(pk)
        data = request.data
        data["survey"] = survey_id
        serializer = QuestionSerializer(question, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        question = self.get_object(pk)
        question.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ChoiceCreate(APIView):

    def post(self, request, question_id):
        data = request.data
        data["question"] = question_id
        serializer = ChoiceSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChoiceDetail(APIView):

    def get_object(self, pk):
        try:
            return Choice.objects.get(pk=pk)
        except Choice.DoesNotExist:
            raise Http404


    def put(self, request, question_id, pk):
        choice = self.get_object(pk)
        data = request.data
        data["question"] = question_id
        serializer = ChoiceSerializer(choice, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        choice = self.get_object(pk)
        choice.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ResponderCreate(APIView):


    def post(self, request, survey_id):
        data = request.data
        data["survey"] = survey_id
        serializer = ResponderSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResponderUpdate(APIView):

    def get_object(self, pk):
        try:
            return Responder.objects.get(pk=pk)
        except Responder.DoesNotExist:
            raise Http404

    def put(self, request, survey_id, pk):
        data = request.data
        data["survey"] = survey_id
        data["submit_time"] = timezone.now()
        responder = self.get_object(pk)
        serializer = ResponderSerializer(responder, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class RespondCreate(APIView):



    def get(self, request, survey_id, responder_id):
        #data = request.data
        """
        print(data)
        for i in range(len(data)):
            data[i]["survey"] = survey_id
            data[i]["responder"] = responder_id


        responder_data = {}
        responder_data["survey"] = survey_id
        responder_data["submit_time"] = timezone.now()
        responder = self.get_object(responder_id)
        serializer = ResponderSerializer(responder, data=responder_data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        """
        survey = Survey.objects.prefetch_related("questions", "responders").prefetch_related("questions__choices").get(pk=survey_id)
        data1=[{"question" : 31, "text" : "hi", "responder" : 6},
              {"question" : 34, "choice" : 4, "responder" : 6},
               {"question" : 30, "rate" : 4, "responder" : 6}]

        serializer = RespondSerializer(data=data1, many=True, context={'survey': survey})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
