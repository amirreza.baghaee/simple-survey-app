from django.urls import path
from survey import views

urlpatterns = [
    path('surveys/', views.SurveyList.as_view()),
    path('surveys/<int:pk>/', views.SurveyDetail.as_view()),
    path('surveys/<int:survey_id>/questions/', views.QuestionList.as_view()),
    path('surveys/<int:survey_id>/questions/<int:pk>/', views.QuestionDetail.as_view()),
    path('surveys/<int:survey_id>/responders/', views.ResponderCreate.as_view()),
    path('surveys/<int:survey_id>/responders/<int:pk>/', views.ResponderUpdate.as_view()),
    path('surveys/<int:survey_id>/responderrs/<int:responder_id>/', views.RespondCreate.as_view()),
    path('surveys/<int:question_id>/choices/', views.ChoiceCreate.as_view()),
    path('surveys/<int:question_id>/choices/<int:pk>/', views.ChoiceDetail.as_view()),

]