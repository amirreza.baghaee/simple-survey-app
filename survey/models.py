from django.db import models, transaction
from django.db.models import F, Model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


class Survey(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        ordering = ['id']

class Question(models.Model):

    class QuestionType(models.TextChoices):
        choice = 'choice'
        text = 'text'
        rate = 'rate'

    title = models.CharField(max_length=500)
    type = models.CharField(max_length=50, choices=QuestionType.choices, default=QuestionType.text)
    order = models.IntegerField(default=1)
    rate = models.IntegerField(null=True, blank=True)
    survey = models.ForeignKey(Survey, related_name='questions', on_delete=models.CASCADE)
    __original_order = None

    class Meta:
        ordering = ['order']

    def __init__(self, *args, **kwargs):
        super(Question, self).__init__(*args, **kwargs)
        self.__original_order = self.order


    def save(self, *args, **kwargs):
        if(self.order != self.__original_order or self.pk is None):
            with transaction.atomic():
                if Question.objects.filter(order=self.order, survey=self.survey).exclude(pk=self.pk).count() > 0:
                    if(self.pk is None):
                        query_set = Question.objects.filter(survey=self.survey).filter(order__gte=self.order)
                        query_set.update(order=F('order') + 1)
                    elif(self.order <self.__original_order):
                        query_set = Question.objects.filter(survey=self.survey).filter(order__gte=self.order).filter(order__lt=self.__original_order)
                        query_set.update(order=F('order') + 1)
                    else:
                        query_set = Question.objects.filter(survey=self.survey).filter(order__lte=self.order).filter(order__gt=self.__original_order)
                        query_set.update(order=F('order') - 1)



        super(Question, self).save(*args, **kwargs)
        self.__original_order = self.order

    def delete(self, *args, **kwargs):
        with transaction.atomic():
            query_set = Question.objects.filter(survey=self.survey).filter(order__gte=self.order)
            query_set.update(order=F('order') - 1)
        super(Question, self).delete(*args, **kwargs)



class Choice(models.Model):
    question = models.ForeignKey(Question, related_name='choices', on_delete=models.CASCADE)
    text = models.CharField(max_length=200)


class Responder(models.Model):

    class ResponderStage(models.TextChoices):
        One = 1
        Two = 2

    start_time = models.DateTimeField(auto_now_add=True)
    submit_time = models.DateTimeField(null=True, blank=True)
    survey = models.ForeignKey(Survey, related_name='responders',on_delete=models.CASCADE)
    stage = models.IntegerField(default=ResponderStage.One, choices=ResponderStage.choices)

    def save(self, *args, **kwargs):
        if(self.pk is not None):
            self.stage = 2
        super(Responder, self).save(*args, **kwargs)


class Respond(models.Model):
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE, null=True,  blank=True)
    rate = models.IntegerField(null=True, blank=True)
    text = models.CharField(max_length=500, null=True,  blank=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    responder = models.ForeignKey(Responder, on_delete=models.CASCADE)
    #todo:validator for rate



