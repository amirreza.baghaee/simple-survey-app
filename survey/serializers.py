from rest_framework import serializers
from survey.models import Survey, Question, Choice, Respond, Responder
from django.db import connection


class SurveySerializer(serializers.ModelSerializer):

    class Meta:
        model = Survey
        fields = "__all__"
        #todo : delete __all__


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = "__all__"
        #todo : delete __all__


class ChoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Choice
        fields = "__all__"
        #todo : delete __all__







    """"
    def validate(self, data):
        print(data)
        keys = list(data.keys())
        if data["question"].type != keys[0]:
            raise serializers.ValidationError("The answer does not math the question type!")
        if keys[1] == "choice":
            qs = Choice.objects.filter(pk=data["choice"].id).filter(question_id=data["question"].id)
            if not qs.exists():
                raise serializers.ValidationError("choice is not valid")
        elif keys[1] == "rate":
            if data["rate"] > data["question"].rate or data["rate"] < 0:
                raise serializers.ValidationError("rate is not valid")

        return data
    """



class ResponderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Responder
        fields = "__all__"
        #todo : delete __all__


class FullQuestionSerializer(serializers.ModelSerializer):

    choices = ChoiceSerializer(many=True)


    class Meta:
        model = Question
        fields = ["id", 'title', 'type', 'order', "rate", "survey", "choices"]






class SurveyDetailSerializer(serializers.ModelSerializer):
    views = serializers.IntegerField()
    responses = serializers.IntegerField()
    class Meta:
        model = Survey
        fields = ['id', 'name', 'views', 'responses']


class FullSurveySerializer(serializers.ModelSerializer):
    questions = FullQuestionSerializer(many=True)
    class Meta:
        model = Survey
        fields = ['id', 'name', 'questions']

class RespondSerializer(serializers.ModelSerializer):
    class Meta:
        model = Respond
        fields = "__all__"
        #todo : delete __all__
"""
    def to_internal_value(self, data):
        for
"""